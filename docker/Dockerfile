FROM gitlab-registry.cern.ch/lhcb-docker/os-base/centos7-devel:latest

ARG IMAGE_NAME=centos7-build

RUN mkdir -p /opt && \
    if [ "$IMAGE_NAME" != "slc5-build" ] ; then \
    case ${IMAGE_NAME} in centos7*) cctools_os=centos7 ;; slc6*) cctools_os=centos6 ;; esac && \
    curl http://ccl.cse.nd.edu/software/files/cctools-7.0.14-x86_64-${cctools_os}.tar.gz | tar -x -z -C /opt -f - && \
    ln -s cctools-7.0.14-x86_64-${cctools_os} /opt/cctools ; fi

# this part is inspired (partially copied) from
# https://github.com/eclipse/che-dockerfiles/blob/master/recipes/stack-base/centos/Dockerfile

# Note: the original Recipe for Eclipse CHE had:
#   USER user
#   WORKSPACE /projects
# I do not know if it's really needed

RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    echo "%root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    echo -e "\tGSSAPIDelegateCredentials yes" >> /etc/ssh/ssh_config && \
    groupadd -g 9999 lhcb && \
    useradd -u 9999 -g lhcb -m -d /userhome -G root,wheel,users lhcb && \
    sed -i 's/requiretty/!requiretty/g' /etc/sudoers && \
    touch /userhome/.sudo_as_admin_successful && \
    chmod 777 /userhome && chown -R 9999:9999 /userhome && \
    rm -f /etc/security/limits.d/*-nproc.conf /tmp/group && \
    chmod 4755 /usr/sbin/{user,group}{add,mod}

USER lhcb
WORKDIR /workspace
ENV HOME /userhome

# If the user/group id does not exist, we will need to replace the content of
# /etc/passwd, /etc/group, etc..
RUN for f in "/etc/passwd" "/etc/group" "/workspace"; do \
    sudo chgrp 0 ${f} && \
    sudo chmod a+rwX ${f}; \
    done

COPY docker/entrypoint.sh /etc/entrypoint.sh
COPY docker/lhcb.sh /etc/profile.d/lhcb.sh
ENTRYPOINT ["/etc/entrypoint.sh"]
CMD []
