# LbDocker

## Description

This repository hosts contextualization scripts and Dockerfiles used to build
Docker images for LHCb software development and execution on supported
platforms (SLC5, SLC6 and CentOS7).

The images built and hosted here are based on the images at
<https://gitlab.cern.ch/lhcb-docker/os-base>.

In addition to Docker support files, we provide a helper command to easily
start customized shells in such containers: `lb-docker-run`.

## `lb-docker-run`

The `lb-docker-run` command wraps a call to `docker run` (see [Docker documentation](https://docs.docker.com/engine/reference/run/)) passing to it
the arguments and options needed to communicate with the contextualization
engine inside the container being started.

By default, when calling `lb-docker-run`, the user is drop in a shell (bash on
SLC6) with the default LHCb user environment, with access to the current
directory via the special path `/workspace` and to the distributed filesystem
'/cvmfs' (see [relative documentation](https://cernvm.cern.ch/portal/filesystem)).
The user inside the container has the same name and id of the user that started
it, but the home directory is a temporary directory removed when leaving the
container.

There are several options to tune the behaviour of `lb-docker-run` (see
`--help`). For example, it is possible to use a persistent home directory
(`--home`) or to disable the LHCb environment (`--no-lblogin`).

## Usage

To use the containers and `lb-docker-run` the minimal requirements are:
- Python 2.6
- Docker allowing the user to start a container

It's better to have [CVMFS](https://cernvm.cern.ch/portal/filesystem) installed on the host machine, but it's not
mandatory.

Then download <https://gitlab.cern.ch/lhcb-core/LbDocker/raw/master/scripts/lb-docker-run> and run it, for example (assuming `~/.local/bin` is in the path):
```bash
curl -o ~/.local/bin/lb-docker-run https://gitlab.cern.ch/lhcb-core/LbDocker/raw/master/scripts/lb-docker-run
chmod a+x ~/.local/bin/lb-docker-run
cd ~/work
lb-docker-run
```
